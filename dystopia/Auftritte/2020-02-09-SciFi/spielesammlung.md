# Sammlung an Spielen und ähnlichem

## Aufwärmen

* Wir spielen, und das Publikum begleitet das mit Gerschäuschen (?)
* Viele kleine Szenen mit Technology (ala Kegeln in der Kirche).
* Assoziationen zu SciFi mit dem Publikum

## SciFi-Specific


### Gedicht von Alien

Ein "Gromolo" Gedicht, gehalten von einem Alien

Vorgaben:

* anatomische Eigenschaft des Aliens
* Was "bewegt" die Aliens (Thema des Gedichts).

Geschätzte Zeit: 10min


### Robo-Ratespiel

Ein Ratespiel. Ein "Robotorjäger" muss den Roboter identifizieren.

Vorgaben:

* Ort?
* Worte die verwendet werden müssen

Varianten:

* Anstatt der Worte, andere Eigenart des Roboters (haben wir noch nicht erforscht).

Geschätzte Zeit: 15min


### Zeitreise

In einer ersten Szene macht der Protagonist etwas, was er gerne im Nachinein anders gemacht hätte.
Er benutzt eine Zeitmaschine um den Fehler zu korrigieren. Dabei wird es schlimmer.

Vorgaben:

* Etwas, was man erledigen muss


Varianten:

* mit oder ohne Singen


Geschätzte Zeit: 15min


### SciFi Song

Wir singen zu einem SciFi Thema

Vorgaben:

* Name des Planetens
* Was ist anders auf dem Planeten
* Was ist ein Held des Planeten
* Worauf sind die Planentenbewohner stolz?

Varianten:

* Nationalhymne von Aliens
* Volkslied von Aliens

Geschätzte Zeit: 10min


## Langformen

### Musical

Ein Musical in einem SciFi Setting

Vorgaben:

* Titel

Varianten:

* Wie letztes mal
* Musical durchspielen ohne Regisseur

Geschätzte Zeit: 25min


### "StarTrek"

Plotvorgabe: Menschen auf einem Raumschiff begegnen Aliens auf ihrem Planeten.

Vorgaben:

* Name des Planetens
* Was ist da anders

Geschätzte Zeit: 25min


### Planet wo was anders ist

Heldenreise auf einem Planeten wo etwas anders ist als bei uns.

Vorgaben:

* Name des Planetens
* Was ist da anders

Geschätzte Zeit: 25min


## Nicht SciFi spezifische Spiele

Wir machen die Vorgaben SciFi!

### Blind Syncro

2 Spieler sprechen, sehen aber die Bühne dabei nicht. 2 Spieler spielen, müssen das gesagte erfüllen.

SciFi Idee: Kann dadurch begründet werden, dass die Spielenden die Avatare sind.

Mag jemand das Spiel: Eher nicht.

Vorgaben: Unspzifiziert

Geschätzte Zeit: 10min


### Stunt Double

2-3 Spieler spielen eine Szene. Sie können jederzeit "Stund Double" Rufen (Avatar?), dann müssen die Stunt Doubles übernehmen, bis die Spieler wieder rein wollen.

SciFi Idee: Die Stunt Double sind Avatare. Wir thematisieren die gesaltschaftliche Implikation, dass man immer seinen Avatar einsetzen kann.

Vorgaben: Brenzlige Situation.

Geschätzte Zeit: 10min


### A-spricht-B-spricht-C

Kennt ihr alle!

SciFi Idee: Wir erzählen ein Märchen in einem SciFi setting.

Vorgabe: SciFi Märchen

Geschätzte Zeit: 15min


### Papgei

Ich habe keine Ahnung


### Mikro-Makro Kosmos

Wir spielen 2 Szenen auf einer geteilten Bühne parallel, es ist die selbe Szene aber einmal aus der Perspektive von etwas sehr kleinem und aus der Perspektive von etwas großem (Beispiel Mensch, Kakerlake).

SciFi Idee: Das kleine ist eine "SciFi-Character" (Saugroboter zum Beispiel).

Vorgabe: Das Kleine und das Große.

Geschätzte Zeit: 10min


### Replay

Ein Replay halt

Geschätzte Zeit: 15min
