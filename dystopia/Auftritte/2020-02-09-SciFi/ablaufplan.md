# Ablaufplan SciFi Auftritt von Dystopia am 9.2.2020


## 1. Halbzeit (1h)

1. Anmoderation (10min)
2. Planet wo was anders ist (25min)
3. Zeitreise (15min)
4. SciFi Song (10min)

## 2. Halbzeit (50min)

1. Gedicht von Alien(10min)
2. ABC (15min)
3. Falls Zeit: Robo-Ratespiel (15min)
4. Musical (20min)

