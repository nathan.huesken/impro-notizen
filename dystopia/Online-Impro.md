# Notizen und Gedanken zum Thema Online-Impro

## Bewertung der getesteten Lösungen

Skype
* :thumbsup: Reihenfolge einstellbar
* :-1: Spiegelung kann man nicht deaktivieren

Zoom
* :thumbsup: Bessere Bild- und Tonqualität
* :thumbsup: Whiteboard zum rummalen
* :thumbsup: Spiegelung kann nicht ausgeschaltet werden

Jitsi
* TBD

## Wünsche einer Optimallösung

* Reihenfolge der Videos soll konfigurierbar sein
* Idealerweise kann man aktiv spielende direkt nebeneinander anordnen
* Alle Bilder sind gespiegelt
* Audioübertragung von mehreren gleichzeitig möglich (zum Singen und für Hintergrundmusik)
* (Geringere Latenz bei der Audioübertragung)

## Ideen

* Audio auf anderem Wege abbilden (z. B. Teamspeak, Mumble)
* Virtuelles spielen auf einer virtuellen Bühne
  * [Second Life](http://go.secondlife.com/) (freemium)
  * [Don't starve](https://www.klei.com/games/dont-starve) (8,19 €)
  * [Counter-Strike](https://store.steampowered.com/app/240/CounterStrike_Source/) (8,19 €) 
